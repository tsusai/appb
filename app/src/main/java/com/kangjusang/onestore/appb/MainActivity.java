package com.kangjusang.onestore.appb;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    /**
     * SharedPreference에 저장하는 데이터 각각에 대한 key 값
     */
    public final static String SP_KEY_GMAIL = "gmail_address";
    public final static String SP_KEY_CARRIER = "carrier";
    public final static String SP_KEY_IS_TEST_MODE = "is_test_mode";

    /**
     * SharePreference에 저장된 데이터 뭉치를 불러오기 위한 name 값
     */
    public final static String SP_NAME_TEST = "shared_preference_test";

    private Activity activity;

    private SwitchCompat switchTestMode;
    private RadioGroup rgCarrier;
    private EditText etGmailAddress;
    private Button btnSave;

    private Utility.CARRIER carrierSelected = Utility.CARRIER.NotSelected;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        TextView tvToolbarTitle = (TextView)findViewById(R.id.tv_toolbar_title);
        setSupportActionBar(toolbar);

        ActionBar supportActionBar = getSupportActionBar();
        tvToolbarTitle.setText(supportActionBar.getTitle());
        supportActionBar.setDisplayShowTitleEnabled(false);
        supportActionBar.setDisplayHomeAsUpEnabled(true);

        initUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sample, menu);
        return true;
    }

    private void initUI() {

        switchTestMode =  (SwitchCompat)findViewById(R.id.switchTestMode);

        rgCarrier = (RadioGroup)findViewById(R.id.radioGroupCarrier);
        rgCarrier.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.radioButtonSKT:
                        carrierSelected = Utility.CARRIER.SKT;
                        break;
                    case R.id.radioButtonKT:
                        carrierSelected = Utility.CARRIER.KT;
                        break;
                    case R.id.radioButtonUPlus:
                        carrierSelected = Utility.CARRIER.UPlus;
                        break;
                    case R.id.radioButtonNotApplicable:
                        carrierSelected = Utility.CARRIER.NotApplicable;
                        break;
                    default:
                        carrierSelected = Utility.CARRIER.NotSelected;
                        break;
                }
            }
        });

        etGmailAddress = (EditText)findViewById(R.id.etGmailAddress);
        etGmailAddress.setHint("Enter g-mail address");

        btnSave = (Button)findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                saveTestData();
            }
        });
    }

    private void saveTestData() {

        // 통신사 확인
        if (carrierSelected == Utility.CARRIER.NotSelected) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            AlertDialog alertDialog = builder.setTitle("통신사 선택").setMessage("통신사를 선택하세요.").setPositiveButton("확인",null).setCancelable(true).create();
            alertDialog.show();
            return;
        }

        // 구글 계정 확인
        String gmailAddress = etGmailAddress.getText().toString();
        if (TextUtils.isEmpty(gmailAddress)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            AlertDialog alertDialog = builder.setTitle("구글 계정").setMessage("구글 계정을 입력해 주세요.").setPositiveButton("확인",null).setCancelable(true).create();
            alertDialog.show();
            return;
        } else {
            if (!gmailAddress.toLowerCase().replace(" ","").endsWith("@gmail.com")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                AlertDialog alertDialog = builder.setTitle("구글 계정").setMessage("구글 계정만 입력할 수 있습니다.").setPositiveButton("확인",null).setCancelable(true).create();
                alertDialog.show();
                return;
            }
        }
        gmailAddress = gmailAddress.substring(0,gmailAddress.toLowerCase().lastIndexOf("@gmail.com")) + "@gmail.com";

        // 저장
        SharedPreferences sharedPreferences = getSharedPreferences(SP_NAME_TEST, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SP_KEY_IS_TEST_MODE, switchTestMode.isChecked());
        editor.putInt(SP_KEY_CARRIER, carrierSelected.getId());
        editor.putString(SP_KEY_GMAIL, gmailAddress);
        editor.commit();

        // 저장 완료
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        AlertDialog alertDialog = builder.setTitle("저장 완료").setMessage("테스트용 데이터가 저장되었습니다.").setPositiveButton("확인",null).setCancelable(true).create();
        alertDialog.show();

    }
}
