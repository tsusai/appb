package com.kangjusang.onestore.appb;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

import static android.content.Context.MODE_PRIVATE;
import static com.kangjusang.onestore.appb.MainActivity.SP_KEY_CARRIER;
import static com.kangjusang.onestore.appb.MainActivity.SP_KEY_GMAIL;
import static com.kangjusang.onestore.appb.MainActivity.SP_KEY_IS_TEST_MODE;
import static com.kangjusang.onestore.appb.MainActivity.SP_NAME_TEST;

/**
 * 테스트용 데이터를 다른 앱에 제공하기 위한 Provider
 */
public class TestDataProvider extends ContentProvider {

    static final String PROVIDER_NAME = "com.kangjusang.onestore.appb.TestDataProvider";

    static final int uriCode = 1;
    static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "users", uriCode);
        uriMatcher.addURI(PROVIDER_NAME, "users/*", uriCode);
    }

    public TestDataProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public String getType(Uri uri) {

        switch (uriMatcher.match(uri)) {
            case uriCode:
                return "vnd.android.cursor.dir/users";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        MatrixCursor matrixCursor = new MatrixCursor(new String[]{"carrier","gmail_address"});

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(SP_NAME_TEST, MODE_PRIVATE);
        Boolean isTestModeOn = sharedPreferences.getBoolean(SP_KEY_IS_TEST_MODE, false);
        if (isTestModeOn) {
            int carrier = sharedPreferences.getInt(SP_KEY_CARRIER, 0);
            String gmailAddress = sharedPreferences.getString(SP_KEY_GMAIL, null);
            Object[] objects = new Object[2];
            objects[0] = carrier;
            objects[1] = gmailAddress;
            matrixCursor.addRow(objects);
        }
        return matrixCursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        return 0;
    }
}
