package com.kangjusang.onestore.appb;

import android.text.TextUtils;

public class Utility {

    public enum CARRIER {

        NotSelected(-1,""),
        SKT(0,"SKTelecom"),
        KT(1,"KT"),
        UPlus(2,"LG U+"),
        NotApplicable(3, "N/A");

        private int mId;
        private String mVisibleName;

        CARRIER(int id, String visibleName) {
            this.mId = id;
            this.mVisibleName = visibleName;
        }

        public int getId() {
            return mId;
        }
        public String getVisibleName() {
            return mVisibleName;
        }

        public static CARRIER getCarrierById(int id) {
            for (CARRIER carrier : CARRIER.values()) {
                if (carrier.mId == id)
                    return carrier;
            }
            return NotSelected;
        }

        public static CARRIER getCarrierByVisibleName(String visibleName) {
            for (CARRIER carrier : CARRIER.values()) {
                if (carrier.mVisibleName.equals(visibleName))
                    return carrier;
            }
            return NotApplicable;
        }
    };

    public static String validateGmailAddress(String gmailAddress) {
        if (TextUtils.isEmpty(gmailAddress)) {
            return null;
        } else {
            if (!gmailAddress.toLowerCase().endsWith("@gmail.com")) {
                return null;
            }
        }
        gmailAddress = gmailAddress.substring(0,gmailAddress.toLowerCase().lastIndexOf("@gmail.com")) + "@gmail.com";
        return gmailAddress;
    }
}
